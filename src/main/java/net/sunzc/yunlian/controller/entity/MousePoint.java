package net.sunzc.yunlian.controller.entity;

import javafx.scene.input.MouseEvent;

/**
 * Created by Sun on 2016/6/11.
 */
public class MousePoint extends Point {
    private static final String TAG = "MousePoint";
    private static MousePoint point;

    private MousePoint(MouseEvent event, double rate) {
        this.rate = rate;
        rawX = (int) event.getX();
        rawY = (int) event.getY();
        this.x = transferPoint(rawX);
        this.y = transferPoint(rawY);
    }

    public static MousePoint getPoint(MouseEvent event, double rate) {
        if (point == null) {
            point = new MousePoint(event, rate);
        } else {
            point.setRate(rate);
            point.setRawX((int) event.getX());
            point.setRawY((int) event.getY());
        }
        return point;
    }
}
