package net.sunzc.yunlian.controller.entity;

/**
 * Created by Sun on 2016/6/9.
 */
public class Point {
    int x, y, rawX, rawY;
    double rate;

    Point() {
    }

    void setRawX(int rawX) {
        this.rawX = rawX;
        this.x = transferPoint(rawX);
    }

    int transferPoint(int raw) {
        return Math.toIntExact(Math.round(raw * rate));
    }

    void setRawY(int rawY) {
        this.rawY = rawY;
        this.y = transferPoint(rawY);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", rawX=" + rawX +
                ", rawY=" + rawY +
                ", rate=" + rate +
                '}';
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
