package net.sunzc.yunlian.controller.entity;

import com.android.ddmlib.Log;
import javafx.scene.image.Image;
import net.sunzc.yunlian.utils.FileUtils;
import net.sunzc.yunlian.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Created by Sun on 2016/6/19.
 */
public class CircleDirectory {
    public static final String ZIP_FILE_NAME = "circlezip.zip";
    private static final String DIR_NAME = "\\circle\\";
    private static final String TAG = "CircleDirectory";
    private static Path mCirclePath;
    private static File mCircleDirectoryFile;
    private static CircleDirectory circleDirectory = new CircleDirectory();
    private static HashMap<String, ImageFile> imageFiles = new HashMap<>();
    private static HashMap<String, TextFile> textFiles = new HashMap<>();
    private static Path zipPath;
    private String text;

    private CircleDirectory() {
        mCirclePath = Paths.get(System.getProperty("user.dir") + DIR_NAME);
        Log.i(TAG, "--------" + mCirclePath.toString());
        try {
            if (Files.exists(mCirclePath) && Files.isDirectory(mCirclePath)) {
                FileUtils.deleteFolder(mCirclePath);
            }
            Files.createDirectories(mCirclePath);
            zipPath = Paths.get(mCirclePath.getParent().toString());
            Files.createDirectories(zipPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, Files.exists(mCirclePath.toAbsolutePath()) + "当前目录是否存在" + mCirclePath.toString());
        mCircleDirectoryFile = mCirclePath.toFile();
    }

    public static CircleDirectory get() {
        return circleDirectory;
    }

    public File[] getFiles() {
        if (mCircleDirectoryFile == null)
            return null;
        return mCircleDirectoryFile.listFiles();
    }

    private void addFile(File file, String name) {
        Path destFile = Paths.get(mCirclePath.toString() + name);
        try {
            Files.deleteIfExists(destFile);
            Files.createFile(destFile);
            FileUtils.copy(file, destFile.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void putImageFile(ImageFile file) {
        Log.i(TAG, file.toString());
        addFile(file.imageFile, file.getName());
        imageFiles.put(file.getName(), file);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getZipFile() {
        String zipFilePath = zipPath.toString() + "\\" + ZIP_FILE_NAME;
        ZipCompresser.zip(getFiles(), zipFilePath, getText());
        clear();
        return zipFilePath;
    }

    public int size() {
        File[] files = getFiles();
        if (files == null) return 0;
        return files.length;
    }

    private void clear() {
        File[] files = getFiles();
        if (files == null) return;
        for (File file : files) {
            try {
                Files.delete(file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void delFile(CircleFile file) {
        try {
            Files.delete(file.getPath());
            if (file instanceof TextFile && textFiles.containsKey(file.getName()))
                textFiles.remove(file.getName());
            if (file instanceof ImageFile && imageFiles.containsKey(file.getName())) {
                imageFiles.remove(file.getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return getFiles().toString() + "----" + text;
    }

    interface CircleFile {
        File getFile();

        Path getPath();

        String getName();
    }

    public static class ImageFile implements CircleFile {
        private static final String IMAGE_FILE_NAME = "/image_";
        private String name;
        private File imageFile;
        private Image image;

        public ImageFile(File file, String name) {
            imageFile = file;
            this.name = IMAGE_FILE_NAME + name;
            image = new Image(imageFile.toURI().toString());
        }

        public String getName() {
            return name;
        }

        public File getFile() {
            return imageFile;
        }

        @Override
        public Path getPath() {
            return imageFile.toPath();
        }

        public Image getImage() {
            return image;
        }

        @Override
        public String toString() {
            return "ImageFile{" +
                    "name='" + name + '\'' +
                    ", imageFile=" + imageFile +
                    ", image=" + image +
                    '}';
        }
    }

    public static class TextFile implements CircleFile {
        private static final String TEXT_FILE_NAME = "/text";
        private String name, content;
        private File textFile;

        TextFile(String name, String content) {
            this.name = Utils.isEmpty(name) ? TEXT_FILE_NAME : name;
            this.content = content;
            Path textPath = Paths.get(mCirclePath.toString() + this.name);
            try {
                Files.deleteIfExists(textPath);
                Files.createFile(textPath);
                try (PrintWriter out = new PrintWriter(textPath.toFile())) {
                    out.println(content);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.textFile = textPath.toFile();
        }

        public TextFile(String content) {
            this("", content);
        }

        public String getName() {
            return name;
        }

        public String getContent() {
            return content;
        }

        public File getFile() {
            return textFile;
        }

        @Override
        public Path getPath() {
            return textFile.toPath();
        }

        @Override
        public String toString() {
            return "TextFile{" +
                    "name='" + name + '\'' +
                    ", content='" + content + '\'' +
                    ", textFile=" + textFile +
                    '}';
        }
    }
}
