package net.sunzc.yunlian.controller;

import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.util.LinkedList;

/**
 * 群控
 * <p>
 * Created by Sun on 2016/6/9.
 */
public class GroupController {
    private static GroupController instance = new GroupController();
    private static LinkedList<GroupControllable> groupControllableList = new LinkedList<>();
    private GroupControllable controller;

    private GroupController() {
    }


    public static void register(GroupControllable groupControllable) {
        groupControllableList.add(groupControllable);
    }

    public static void unregister(GroupControllable groupControllable) {
        if (groupControllable != null && groupControllableList.contains(groupControllable))
            groupControllableList.remove(groupControllable);
    }

    public static GroupController getController() {
        return instance;
    }

    public void setController(GroupControllable groupControllable) {
        if (this.controller != null) {
            this.controller.releaseController();
        }
        this.controller = groupControllable;
        if (groupControllable != null) {
            groupControllable.asController();
        }
    }

    public void cancelControl() {
        this.setController(null);
    }

    /**
     * 领控，当前
     * 没有，有 只控制一台机器
     * 没有，没有 不控制，也不会出发控制，初始化状态
     * 有，有 这两个可以是一台机器，也可以不是一台机器，但是无论怎样，只有领控机器触发控制，其他机器不能出发控制
     * 有，同一台 领控机器出发控制，其他机器被通知
     * 有，不是同一台，领控机器触发控制，其他机器被通知
     *
     * @param event
     */
    public void onMousePressed(MouseEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onMousePressed(event);
        });
    }

    private boolean hasController() {
        return controller != null;
    }

    public void onMouseReleased(MouseEvent event) {//如果不是控制者则执行响应的操作
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onMouseReleased(event);
        });
    }

    public void onMouseDragged(MouseEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onMouseDragged(event);
        });
    }

    public void sendCircle(String file) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.sendCircle(file);
        });
    }

    public boolean isController(GroupControllable groupControllable) {
        return hasController() && this.controller.equals(groupControllable);
    }

    public void onKeyReleased(KeyEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onKeyReleased(event);
        });
    }

    public void onLogin(ActionEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onLogin(event);
        });
    }

    public void onBackPressed(ActionEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onBackPressed(event);
        });
    }

    public void onStartClient(ActionEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onStartClient(event);
        });
    }

    public void onLockPressed(ActionEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onLockPressed(event);
        });
    }

    public void onHomePressed(ActionEvent event) {
        if (!hasController()) return;
        groupControllableList.stream().filter(groupControllable -> !isController(groupControllable)).forEach(groupControllable -> {
            groupControllable.onHomePressed(event);
        });
    }
}
