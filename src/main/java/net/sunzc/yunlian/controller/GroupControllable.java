package net.sunzc.yunlian.controller;

import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 * 可群控的接口
 *
 * @author Administrator
 * @date 2016/6/7
 */
public interface GroupControllable {

    void onMouseDragged(MouseEvent event);

    void onMouseReleased(MouseEvent event);

    void onMousePressed(MouseEvent event);

    void onKeyReleased(KeyEvent event);

    void releaseController();

    void asController();

    void sendCircle(String filePath);

    void onLogin(ActionEvent event);

    void onBackPressed(ActionEvent event);

    void onHomePressed(ActionEvent event);

    void onStartClient(ActionEvent event);

    void onLockPressed(ActionEvent event);
}
