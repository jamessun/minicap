package net.sunzc.yunlian.view;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.Log;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import net.sunzc.yunlian.interact.device.CommandInteract;
import net.sunzc.yunlian.interact.device.command.ActionCommand;
import net.sunzc.yunlian.interact.device.factory.AdbInputCommandFactory;
import net.sunzc.yunlian.utils.Utils;

import java.io.IOException;

import static com.android.SdkConstants.TAG;

/**
 * Created by Sun on 2016/6/17.
 */
public class SendTextField extends HBox {
    private static final int COMMAND_LIMITED = 64;//一次最多只能发送1024个char，可以把char转成十六进制传输
    @FXML
    public TextField inputBox;
    @FXML
    public ChoiceBox<String> shortcuts;
    private CommandInteract commandInteract;

    public SendTextField(CommandInteract device) {
        commandInteract = device;
        DialogUtils.bindFXML(this, "send_text_field.fxml");
        ObservableList<String> shortcutItems = FXCollections.observableArrayList();
        shortcutItems.add("短语一");
        shortcutItems.add("短语二");
        shortcutItems.add("短语三");
        shortcutItems.add("短语四");
        shortcutItems.add("短语五");
        shortcutItems.add("短语六");
        shortcutItems.add("短语七");
        shortcutItems.add("短语八");
        shortcutItems.add("短语九");
        shortcutItems.add("短语十");
        shortcuts.setItems(shortcutItems);
        shortcuts.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            inputBox.setText(newValue);
        });
    }

    public void setInputBoxWith(double width) {
        inputBox.setPrefWidth(width);
    }

    @FXML
    public void onInputFinished(KeyEvent event) {
        switch (event.getCode()) {
            case ENTER:
                try {
                    commandInteract.shellExecute(AdbInputCommandFactory.getActionCommand(ActionCommand.Action.SWITCH_IME));
                } catch (TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException | IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "切换输入法失败");
                    return;
                }
                String content = inputBox.getText();
                Utils.threadPool.execute(() -> sendTextsToPc(content));
                inputBox.setText("");
                event.consume();
                break;
            default:
                break;
        }
    }

    private void sendTextsToPc(String content) {
        if (Utils.isEmpty(content)) return;
        String commandText, tailText = null;
        if (content.length() <= COMMAND_LIMITED) {
            commandText = content;
        } else {
            commandText = content.substring(0, COMMAND_LIMITED + 1);
            tailText = content.substring(COMMAND_LIMITED + 1);
        }
        try {
            commandInteract.shellExecute(AdbInputCommandFactory.getTextCommand(commandText));
        } catch (TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException | IOException e) {
            e.printStackTrace();
            Log.e(TAG, "发送文字失败-->" + commandText);
        }
        sendTextsToPc(tailText);
    }
}
