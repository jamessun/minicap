package net.sunzc.yunlian.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;

import java.io.IOException;
import java.io.Serializable;

/**
 * 序号
 * Created by Sun on 2016/6/17.
 */
public class Number extends Label implements Serializable {

    private static final long serialVersionUID = 8871685506644719358L;

    public Number() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("number.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
