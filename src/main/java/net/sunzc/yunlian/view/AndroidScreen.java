package net.sunzc.yunlian.view;

import javafx.scene.image.Image;

/**
 * 手机屏幕
 * Created by Administrator on 2016/7/11.
 */
public interface AndroidScreen {
    /**
     * 投影画面
     *
     * @param image
     */
    void projectScreenImage(Image image);

    /**
     * 更新屏幕的序号
     *
     * @param number
     */
    void updateScreenNumber(int number);
}
