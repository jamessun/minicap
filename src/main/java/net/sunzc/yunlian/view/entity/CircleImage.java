package net.sunzc.yunlian.view.entity;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import net.sunzc.yunlian.controller.entity.CircleDirectory;
import net.sunzc.yunlian.view.DialogUtils;

/**
 * Created by Sun on 2016/6/18.
 */
public class CircleImage extends AnchorPane {
    @FXML
    private ImageView removeBt;
    @FXML
    private ImageView circleImageView;
    private CircleDirectory.ImageFile imageFile;

    public CircleImage(CircleDirectory.ImageFile file) {
        DialogUtils.bindFXML(this, "circle_image.fxml");
        setRightAnchor(removeBt, 5d);
        setTopAnchor(removeBt, 5d);
        imageFile = file;
        circleImageView.setImage(imageFile.getImage());
    }

    public CircleDirectory.ImageFile getImageFile() {
        return imageFile;
    }

    public void setOnRemoveImage(OnRemoveListener listener) {
        removeBt.setOnMouseClicked(event -> {
            if (listener != null) {
                listener.onRemove(CircleImage.this);
            }
        });
    }

    public interface OnRemoveListener {
        void onRemove(CircleImage circleImage);
    }
}
