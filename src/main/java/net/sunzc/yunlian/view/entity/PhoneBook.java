package net.sunzc.yunlian.view.entity;

/**
 * @author Administrator
 * @date 2016/6/7
 */
public class PhoneBook {
    private String user, password;

    public PhoneBook(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PhoneBook{" +
                "user='" + user + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
