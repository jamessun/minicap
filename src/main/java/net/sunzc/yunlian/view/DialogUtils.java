package net.sunzc.yunlian.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import net.sunzc.yunlian.FxApp;
import net.sunzc.yunlian.controller.entity.CircleDirectory;
import net.sunzc.yunlian.view.entity.PhoneBook;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static javafx.scene.control.ButtonBar.ButtonData.OK_DONE;

/**
 * Created by Sun on 2016/6/6.
 */
public class DialogUtils {
    //绑定的fxml

    private static Alert buildDialog(Alert.AlertType alertType, String titleTxt, String headerText, String contentText) {
        Alert alert = new Alert(alertType);
        alert.setTitle(titleTxt);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        return alert;
    }

    public static Alert buildInfoDialog(String titleTxt, String headerText, String contentText) {
        return buildDialog(Alert.AlertType.INFORMATION, titleTxt, headerText, contentText);
    }

    public static Alert buildErrorDialog(String titleTxt, String headerText, String contentText) {
        return buildDialog(Alert.AlertType.ERROR, titleTxt, headerText, contentText);
    }

    public static Optional<ButtonType> showConfirmDialog(String titleTxt, String headerText, String contentText) {
        Alert alert = buildDialog(Alert.AlertType.CONFIRMATION, titleTxt, headerText, contentText);
        return alert.showAndWait();
    }

    public static String showChoiceDialog(String titleTxt, List<String> dialogData) {
        ChoiceDialog<String> dialog = new ChoiceDialog<>(dialogData.get(0), dialogData);
        dialog.setTitle(titleTxt);
        dialog.setHeaderText("Select your choice");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent())
            return result.get();
        else
            return null;
    }

    public static String showInputDialog(String titleText, String headerText, String contentText) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(titleText);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        } else
            return null;
    }

    public static CircleDirectory getCircleDirectory() {
        Dialog<CircleDirectory> dialog = new Dialog<>();
        dialog.setTitle("编辑朋友圈");
        dialog.setResizable(false);
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("yun_lian_logo.png"));
        SendCirclePanel panel = new SendCirclePanel();
        dialog.getDialogPane().setContent(panel);
        ButtonType sendBt = new ButtonType("发送", OK_DONE);
        ButtonType addImageBt = new ButtonType("添加图片", ButtonBar.ButtonData.OTHER);
//        ButtonType closeBt = new ButtonType("取消", ButtonBar.ButtonData.APPLY);
        dialog.getDialogPane().getButtonTypes().addAll(addImageBt, sendBt);
        dialog.setResultConverter(b -> {
            switch (b.getButtonData()) {
                case OK_DONE:
                    return panel.getImagesAndText();
                case OTHER:
                    panel.addImage();
                    break;
            }
            return null;
        });
        Optional<CircleDirectory> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        } else
            return null;
    }

    public static PhoneBook showLoginDialog(String titleTxt, String header) {
        Dialog<PhoneBook> dialog = new Dialog<>();
        dialog.setTitle(titleTxt);
        dialog.setHeaderText(header);
        dialog.setResizable(false);
        Label label1 = new Label("用户名: ");
        Label label2 = new Label("密码: ");
        TextField userTf = new TextField();
        PasswordField passwordTf = new PasswordField();
        GridPane grid = new GridPane();
        grid.add(label1, 1, 1);
        grid.add(userTf, 2, 1);
        grid.add(label2, 1, 2);
        grid.add(passwordTf, 2, 2);
        dialog.getDialogPane().setContent(grid);
        ButtonType buttonTypeOk = new ButtonType("登录", OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
        dialog.setResultConverter(b -> {
            if (b == buttonTypeOk) {
                return new PhoneBook(userTf.getText(), passwordTf.getText());
            }
            return null;
        });

        Optional<PhoneBook> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        } else
            return null;

    }

    public static void bindFXML(Node node, String fxmlName) {
        FXMLLoader fxmlLoader = new FXMLLoader(node.getClass().getClassLoader().getResource(fxmlName));
        fxmlLoader.setRoot(node);
        fxmlLoader.setController(node);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addSystemTray() {
        //添加系统托盘图标.
        SystemTray tray = SystemTray.getSystemTray();
        try {
            BufferedImage image = ImageIO.read(FxApp.class.getResourceAsStream("orange-ball.png"));
            TrayIcon trayIcon = new TrayIcon(image, "自动备份工具");
            trayIcon.setToolTip("自动备份工具");
            tray.add(trayIcon);
            trayIcon.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
        } catch (IOException | AWTException e) {
            e.printStackTrace();
        }

    }
}
