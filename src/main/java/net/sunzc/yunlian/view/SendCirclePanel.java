package net.sunzc.yunlian.view;

import com.android.ddmlib.Log;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import net.sunzc.yunlian.FxApp;
import net.sunzc.yunlian.controller.entity.CircleDirectory;
import net.sunzc.yunlian.view.entity.CircleImage;

import java.io.File;
import java.util.List;

/**
 * 发送朋友圈的面板
 * Created by Sun on 2016/6/18.
 */
public class SendCirclePanel extends VBox implements CircleImage.OnRemoveListener {

    private static final String TAG = "SendCirclePanel";
    @FXML
    private TextArea circleContent;
    @FXML
    private FlowPane circleImages;
    private CircleDirectory circleDirectory;

    SendCirclePanel() {
        DialogUtils.bindFXML(this, "send_circle_panel.fxml");
        circleDirectory = CircleDirectory.get();
    }

    void addImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("添加朋友圈图片");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.dir"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        List<File> files = fileChooser.showOpenMultipleDialog(FxApp.getStage());
        Log.i(TAG, files.toString());
        for (File file : files) {
            CircleDirectory.ImageFile imageFile = new CircleDirectory.ImageFile(file, circleDirectory.size() + ".jpg");
            circleDirectory.putImageFile(imageFile);
            CircleImage circleImage = new CircleImage(imageFile);
            circleImage.setOnRemoveImage(this);
            circleImages.getChildren().add(circleImage);
        }
    }

    @Override
    public void onRemove(CircleImage circleImage) {
        circleDirectory.delFile(circleImage.getImageFile());
        circleImages.getChildren().remove(circleImage);
    }

    CircleDirectory getImagesAndText() {
        //TODO 获得文字和图片，并加密独立保存多个ZipEntry文件，
        String content = circleContent.getText();
        circleDirectory.setText(content);
        Log.i(TAG, "保存完毕" + circleDirectory);
        return circleDirectory;
    }
}
