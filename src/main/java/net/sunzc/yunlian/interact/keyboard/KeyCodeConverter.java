package net.sunzc.yunlian.interact.keyboard;

import com.android.ddmlib.Log;

import java.awt.event.KeyEvent;

public class KeyCodeConverter {

    private static final String TAG = "KeyCodeConverter";

    public static int getKeyCode(KeyEvent e) {
        Log.i(TAG, "getKeyCode(KeyEvent e=" + e + ") - start");
        int code = InputKeyEvent.KEYCODE_UNKNOWN.getCode();
        char c = e.getKeyChar();
        int keyCode = e.getKeyCode();
        InputKeyEvent inputKeyEvent = InputKeyEvent.getByCharacterOrKeyCode(Character.toLowerCase(c), keyCode);
        if (inputKeyEvent != null) {
            code = inputKeyEvent.getCode();
        }
        Log.d(TAG, ("getKeyCode(KeyEvent e=" + e + ") - end"));
        return code;
    }

}
