package net.sunzc.yunlian.interact.device.command;

import net.sunzc.yunlian.controller.entity.Point;

public class SwipeCommand extends InputCommand {
    private static final String COMMAND_FORMAT = INPUT + WHITESPACE + "swipe %d %d %d %d";
    private Point mStartPoint, mEndPoint;

    public SwipeCommand(Point start, Point end) {
        mStartPoint = start;
        mEndPoint = end;
    }

    @Override
    public String getFormatedCommand() {
        return String.format(COMMAND_FORMAT, mStartPoint.getX(), mStartPoint.getY(), mEndPoint.getX(), mEndPoint.getY());
    }

    public void setEndPoint(Point endPoint) {
        this.mEndPoint = endPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.mStartPoint = startPoint;
    }
}
