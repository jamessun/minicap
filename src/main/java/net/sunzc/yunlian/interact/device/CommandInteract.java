package net.sunzc.yunlian.interact.device;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import net.sunzc.yunlian.interact.device.command.Command;

import java.io.IOException;

/**
 * Created by Administrator on 2016/7/11.
 */
public interface CommandInteract {
    String shellExecute(Command command) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException;

    IDevice getDevice();

    void pullFile(String remote, String local);

    void pushFile(String local, String remote);

    void createForward(int localPort, String remoteSocketName,
                       IDevice.DeviceUnixSocketNamespace namespace);

    String getFrameSize();

    String getAbi();

    String getSdk();

    String getSerialNum();

    String getSize();

}
