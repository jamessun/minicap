package net.sunzc.yunlian.interact.device.command;

import net.sunzc.yunlian.controller.entity.Point;

public class TapCommand extends InputCommand {
    private static final String COMMAND_FORMAT = INPUT + WHITESPACE + "tap %d %d";
    private Point point;

    public TapCommand(Point point) {
        this.point = point;
    }

    @Override
    public String getFormatedCommand() {
        return String.format(COMMAND_FORMAT, point.getX(), point.getY());
    }

    public void setPoint(Point point) {
        this.point = point;
    }

}
