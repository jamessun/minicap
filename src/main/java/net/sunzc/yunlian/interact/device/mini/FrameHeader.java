/**
 *
 */
package net.sunzc.yunlian.interact.device.mini;

import net.sunzc.yunlian.utils.Utils;

/**
 * @author hui.qian qianhui@58.com
 * @date 2015年8月10日 下午4:29:42
 */
public class FrameHeader {
    /**
     * 版本号(0 char)
     */
    private int version;
    /**
     * header的尺寸(1 char)
     */
    private int size;
    /**
     * 进程id(2-5 int)
     */
    private int pid;
    /**
     * 用像素来表示的设备宽度(6-9 int)
     */
    private int realWidth;
    /**
     * 用像素来表示的设备高度(10-13 int)
     */
    private int realHeight;
    /**
     * 用像素表示的投影宽度(14-17 int )
     */
    private int virtualWidth;
    /**
     * 用像素来表示的投影高度(18-21 int )
     */
    private int virtualHeight;
    /**
     * 屏幕方向(22 char)
     */
    private int orientation;
    /**
     * 需要注意的(23 char)
     * 1、保证每一帧都能发送出去，即使画面完全相同。可以通过降低读取数据的速度来降低帧率
     * 2、每一帧的方向都是直立的
     * 3、坏掉的帧也有可能会显示
     */
    private int quirks;

    public FrameHeader(byte[] headerBytes) {
//byte转object
        version = headerBytes[0];
        size = headerBytes[1];
        pid = Utils.byteArrayToInt(Utils.subByteArray(headerBytes, 2, 6));
        realWidth = Utils.byteArrayToInt(Utils.subByteArray(headerBytes, 6, 10));
        realHeight = Utils.byteArrayToInt(Utils.subByteArray(headerBytes, 10, 14));
        virtualWidth = Utils.byteArrayToInt(Utils.subByteArray(headerBytes, 14, 18));
        virtualHeight = Utils.byteArrayToInt(Utils.subByteArray(headerBytes, 18, 22));
        orientation = headerBytes[22];
        quirks = headerBytes[23];
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getRealWidth() {
        return realWidth;
    }

    public void setRealWidth(int realWidth) {
        this.realWidth = realWidth;
    }

    public int getRealHeight() {
        return realHeight;
    }

    public void setRealHeight(int realHeight) {
        this.realHeight = realHeight;
    }

    public int getVirtualWidth() {
        return virtualWidth;
    }

    public void setVirtualWidth(int virtualWidth) {
        this.virtualWidth = virtualWidth;
    }

    public int getVirtualHeight() {
        return virtualHeight;
    }

    public void setVirtualHeight(int virtualHeight) {
        this.virtualHeight = virtualHeight;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getQuirks() {
        return quirks;
    }

    public void setQuirks(int quirks) {
        this.quirks = quirks;
    }

    @Override
    public String toString() {
        return "FrameHeader{" +
                "version=" + version +
                ", size=" + size +
                ", pid=" + pid +
                ", realWidth=" + realWidth +
                ", realHeight=" + realHeight +
                ", virtualWidth=" + virtualWidth +
                ", virtualHeight=" + virtualHeight +
                ", orientation=" + orientation +
                ", quirks=" + quirks +
                '}';
    }
}
