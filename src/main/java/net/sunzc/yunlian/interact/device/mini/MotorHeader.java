package net.sunzc.yunlian.interact.device.mini;

import com.android.ddmlib.Log;

import static com.android.SdkConstants.TAG;

/**
 * Created by Administrator on 2016/9/18.
 */
public class MotorHeader {
    private int maxX;
    private int maxY;
    private int maxPoint;
    private int maxPress;
    private int version;
    private int pid;

    public MotorHeader(byte[] bytes) {
        String motorHeader = new String(bytes);
        String array[] = motorHeader.split(" |\n");
        Log.i(TAG, "MotorHeader" + motorHeader);
        version = Integer.valueOf(array[1]);
        maxPoint = Integer.valueOf(array[3]);
        maxPress = Integer.valueOf(array[6]);
        maxX = Integer.valueOf(array[4]);
        maxY = Integer.valueOf(array[5]);
        Log.i(TAG, "收到消息" + this);
    }

    @Override
    public String toString() {
        return "MotorHeader{" +
                "maxX=" + maxX +
                ", maxY=" + maxY +
                ", maxPoint=" + maxPoint +
                ", maxPress=" + maxPress +
                ", version=" + version +
                ", pid=" + pid +
                '}';
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }

    public int getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(int maxPoint) {
        this.maxPoint = maxPoint;
    }

    public int getMaxPress() {
        return maxPress;
    }

    public void setMaxPress(int maxPress) {
        this.maxPress = maxPress;
    }
}
