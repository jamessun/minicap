package net.sunzc.yunlian.interact.device.command;


import net.sunzc.yunlian.interact.keyboard.InputKeyEvent;

public class KeyCommand extends InputCommand {
    private static final String COMMAND_FORMAT = INPUT + WHITESPACE + "keyevent %d";
    private int code;

    public KeyCommand(int keyCode) {
        this.code = keyCode;
    }

    public KeyCommand(InputKeyEvent inputKeyEvent) {
        code = inputKeyEvent.getCode();
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getFormatedCommand() {
        return String.format(COMMAND_FORMAT, code);
    }

}
