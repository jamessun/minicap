package net.sunzc.yunlian.interact.device.mini;

import net.sunzc.yunlian.view.AndroidScreen;

/**
 * 这是手机画面与pc同步投影的投影管理器
 * Created by Administrator on 2016/7/11.
 */
public interface Projector {
    void setProjectScreen(AndroidScreen screen);

    void takeScreenCapture(String absolutePath);

    void stopScreenProjector();

    void restartScreenProjector();
}
