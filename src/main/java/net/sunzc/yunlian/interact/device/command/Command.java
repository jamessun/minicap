package net.sunzc.yunlian.interact.device.command;

public abstract class Command {
    private int timeOut = 5;

    public abstract String getFormatedCommand();

    public int getTimeOut() {
        return timeOut;
    }

    void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }
}
