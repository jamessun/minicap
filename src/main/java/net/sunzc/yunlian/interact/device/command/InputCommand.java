package net.sunzc.yunlian.interact.device.command;

public abstract class InputCommand extends Command {
    /*
          text <string> (Default: touchscreen)
      keyevent [--longpress] <key code number or name> ... (Default: keyboard)
      tap <x> <y> (Default: touchscreen)
      swipe <x1> <y1> <x2> <y2> [duration(ms)] (Default: touchscreen)
     */
    protected static final String INPUT = "input";
    protected static final String WHITESPACE = " ";
    private static final String TO_STRING_PATTERN = "%s [%s]";

    @Override
    public String toString() {
        return String.format(TO_STRING_PATTERN, this.getClass().getSimpleName(), getFormatedCommand());
    }
}
