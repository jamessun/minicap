package net.sunzc.yunlian.interact.device.factory;

import net.sunzc.yunlian.controller.entity.Point;
import net.sunzc.yunlian.interact.device.command.*;
import net.sunzc.yunlian.interact.keyboard.InputKeyEvent;
import net.sunzc.yunlian.utils.Utils;

public final class AdbInputCommandFactory {
    private static final String EXTRAS = " --es ";
    private static final String EXTRAS_NAME = EXTRAS + "name ";
    private static final String EXTRAS_TIME = EXTRAS + "time ";
    private static KeyCommand keyCommand;
    private static TextCommand textCommand;
    private static TapCommand tapCommand;
    private static SwipeCommand swipeCommand;
    private static ActionCommand actionCommand;
    private static BroadcastCommandFactory broadcastCommand;

    public static KeyCommand getKeyCommand(InputKeyEvent inputKeyEvent) {
        if (keyCommand == null)
            keyCommand = new KeyCommand(inputKeyEvent);
        else
            keyCommand.setCode(inputKeyEvent.getCode());
        return keyCommand;
    }

    public static TextCommand getTextCommand(String text) {
        if (textCommand == null)
            textCommand = new TextCommand(text);
        else {
            textCommand.setText(text);
        }
        return textCommand;
    }

    public static ActionCommand getActionCommand(ActionCommand.Action action, String param) {
        if (actionCommand == null) {
            actionCommand = new ActionCommand(action, param);
        } else {
            actionCommand.setAction(action);
            actionCommand.setParam(param);
        }
        return actionCommand;
    }

    public static ActionCommand getSendCircleCommand(String name, int time) {
        String param = "";
        if (!Utils.isEmpty(name))
            param = EXTRAS_NAME + name;
        return getActionCommand(ActionCommand.Action.SEND_BROADCAST, EXTRAS_TIME + time + param);
    }

    public static ActionCommand getActionCommand(ActionCommand.Action action) {
        return getActionCommand(action, null);
    }

    public static TapCommand getTapCommand(Point point) {
        if (tapCommand == null) {
            tapCommand = new TapCommand(point);
        } else {
            tapCommand.setPoint(point);
        }
        return tapCommand;
    }

    public static SwipeCommand getSwipeCommand(Point start, Point end) {
        if (swipeCommand == null) {
            swipeCommand = new SwipeCommand(start, end);
        } else {
            swipeCommand.setStartPoint(start);
            swipeCommand.setEndPoint(end);
        }
        return swipeCommand;
    }

}
