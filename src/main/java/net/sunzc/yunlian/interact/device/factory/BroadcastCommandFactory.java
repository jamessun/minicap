package net.sunzc.yunlian.interact.device.factory;

import net.sunzc.yunlian.interact.device.command.ActionCommand;
import net.sunzc.yunlian.interact.device.command.Command;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sun on 2016/6/26.
 */
public class BroadcastCommandFactory {
    public static Command getBroadcastCommand(BroadcastAction action, Extras extras) {
        return AdbInputCommandFactory.getActionCommand(ActionCommand.Action.SEND_BROADCAST, action.action + extras.toString());
    }

    public enum BroadcastAction {
        SEND_CIRCLE(" net.sunzc.yunlian.ACTION_START_CIRCLE "), START_LOGIN(" net.sunzc.yunlian.START_LOGIN ");
        private final String action;

        BroadcastAction(String action) {
            this.action = action;
        }
    }

    public static class Extras {
        public static final String EXTRAS_NAME = " --es net.sunzc.yunlian.EXTRAS_NAME ";
        public static final String EXTRAS_TIME = " --es net.sunzc.yunlian.EXTRAS_TIME ";
        public static final String EXTRAS_PASSWORD = " --es net.sunzc.yunlian.PASSWORD ";
        public static final String EXTRAS_EMAIL = " --es net.sunzc.yunlian.EMAIL ";
        private HashMap<String, Object> extras = new HashMap<>();

        public void putExtras(String name, Object value) {
            extras.put(name, value);
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<String, Object> entry : extras.entrySet()) {
                stringBuilder.append(entry.getKey()).append(entry.getValue());
            }
            return stringBuilder.toString();
        }
    }

}
