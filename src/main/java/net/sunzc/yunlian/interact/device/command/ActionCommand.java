package net.sunzc.yunlian.interact.device.command;

import net.sunzc.yunlian.utils.Utils;

/**
 * Created by Sun on 2016/6/17.
 */
public class ActionCommand extends Command {

    private Action action;
    private String param;

    public ActionCommand(Action action) {
        this(action, null);
    }

    public ActionCommand(Action action, String param) {
        this.action = action;
        this.param = param;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    @Override
    public String getFormatedCommand() {
        if (Utils.isEmpty(param))
            return action.getName();
        else
            return action.getName() + param;
    }

    public enum Action {
        SWITCH_IME("ime set net.sunzc.yunlian/.AdbIME"),
        SEND_BROADCAST("am broadcast -a "),
        OPEN_CLIENT("am start -a android.intent.action.MAIN -c android.intent.category.LAUNCHER -n net.sunzc.yunlian/net.sunzc.yunlian.MainActivity");
        String name;

        Action(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
