package net.sunzc.yunlian.interact.device.command;

import java.io.UnsupportedEncodingException;

/**
 * 输入文字
 * Created by Sun on 2016/6/9.
 */
public class TextCommand extends InputCommand {
    private static final String COMMAND_FORMAT = "am broadcast -a ADB_INPUT_TEXT --es msg '%s' ";
    private String text;

    public TextCommand(String text) {
        try {
            this.text = toString(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String toString(byte[] a) {
        if (a == null)
            return "null";
        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a[i]);
            if (i == iMax)
                return b.append(']').toString();
            b.append(",");
        }
    }

    public void setText(String text) {
        try {
            this.text = toString(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getFormatedCommand() {
        return String.format(COMMAND_FORMAT, text);
    }
}
