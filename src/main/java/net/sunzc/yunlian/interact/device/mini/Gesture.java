package net.sunzc.yunlian.interact.device.mini;

import net.sunzc.yunlian.controller.entity.Point;

/**
 * Created by Administrator on 2016/7/11.
 */
public interface Gesture {
    void onTouch(Point point);

    void onDown(Point point);

    void onRelease();

    void stopGesture();
    void restartGesture();
}
