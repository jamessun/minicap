package net.sunzc.yunlian.interact.device.command;

/**
 * Created by Administrator on 2016/7/18.
 */
public class CommonCommand extends Command {
    private static CommonCommand commonCommand = new CommonCommand();
    private String command;
    private int timeOut = 5;

    private CommonCommand() {

    }

    public static CommonCommand getCommand(String commandStr) {
        commonCommand.setCommand(commandStr);
        return commonCommand;
    }

    public static CommonCommand getCommand(String commandStr, int timeOut) {
        commonCommand.setCommand(commandStr);
        commonCommand.setTimeOut(timeOut);
        return commonCommand;
    }

    public int getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    private void setCommand(String commandStr) {
        this.command = commandStr;
    }

    @Override
    public String getFormatedCommand() {
        return command;
    }
}
