package net.sunzc.yunlian.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2016/5/31.
 */
public class Utils {
    public static final ExecutorService threadPool = Executors.newCachedThreadPool();

    /**
     * byte[]转int
     *
     * @param bytes
     * @return
     */
    public static int byteArrayToInt(byte[] bytes) {
        int result = 0;
        for (int i = 0; i < bytes.length; i++) {
            result += ((bytes[i] & 0xff) << (i << 3));
        }
        return result;
    }

    public static byte[] subByteArray(byte[] byte1, int start, int end) {
        try {
            byte[] byte2 = new byte[end - start];
            System.arraycopy(byte1, start, byte2, 0, end - start);
            return byte2;
        } catch (NegativeArraySizeException e) {
            e.printStackTrace();
            return null;
        }
    }

    // java合并两个byte数组
    public static byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
        byte[] byte_3 = new byte[byte_1.length + byte_2.length];
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
        return byte_3;
    }

    /**
     * Returns true if the string is null or 0-length.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static int getFreePort() {
        int port = 0;
        try {
            ServerSocket socket = new ServerSocket(0);
            port = socket.getLocalPort();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return port;
    }

    public static Image createImageFromByte(byte[] binaryData) {
        Image image = null;
        InputStream in = new ByteArrayInputStream(binaryData);
        try {
            image = new Image(in);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return image;
    }

    public static void saveToFile(Image image, String path) {
        if (isEmpty(path)) return;
        File outputFile = new File(path);
        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
        try {
            ImageIO.write(bImage, "png", outputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Dimension getScreenSize() {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }

    public static int sumBytes(byte[] bytes, int start, int end) {
        if (end < start || start < 0 || end > bytes.length)
            throw new ArrayIndexOutOfBoundsException("数组越界");
        int result = 0;
        if (end - start == 1) {
            result = bytes[start] & 0xff;
        } else
            for (int index = start; index < end; index++) {
                result += ((bytes[index] & 0xff) << (index << 3));
            }
        return result;
    }
}
