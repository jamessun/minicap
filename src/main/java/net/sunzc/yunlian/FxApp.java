package net.sunzc.yunlian;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.Log;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;


/**
 *
 */
public class FxApp extends Application {
    private static final String TAG = "FxApp";
    public static Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        stage = primaryStage;
        DdmPreferences.setLogLevel(Log.LogLevel.ERROR.getStringValue());
        DdmPreferences.setTimeOut(10 * 1000);
        release(primaryStage);
    }

    private void release(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/main_window.fxml"));
        Log.i(TAG, "xml加载");
        showWindow(primaryStage, root);
    }

    private void showWindow(Stage primaryStage, Parent root) {
        primaryStage.setTitle("云联信息");
        primaryStage.setScene(new Scene(root));
        Image icon = new Image("yun_lian_logo.png");
        primaryStage.setMaximized(true);
        primaryStage.getIcons().add(icon);
        primaryStage.centerOnScreen();
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            AndroidDebugBridge.terminate();
            System.exit(1);
        });
    }
}
